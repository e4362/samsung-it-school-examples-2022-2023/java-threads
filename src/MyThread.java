public class MyThread extends Thread {
    int n;
    int k;
    public MyThread(int n, int k){
        this.n = n;
        this.k = k;
    }
    @Override
    public void run() {
        double arr[] = new double[100];
        long start = System.currentTimeMillis();
        for (long i = n * (10000000000L / k); i < (n + 1) * (10000000000L / k); i++){
            double a = Math.sqrt(i);
            double b = Math.sqrt(i+1);
            double result = a+b;
            arr[(int) (i % 100)] = result;
        }
        long stop = System.currentTimeMillis();
        System.out.println("Thread " + n + " worked for " + (stop - start));
    }
}

public class AnotherThread extends Thread {

    @Override
    public void run() {
        double arr[] = new double[100];
        long start = System.currentTimeMillis();
        for (long i = 0L; i < 10000000000L; i++){
            double a = Math.sqrt(i);
            double b = Math.sqrt(i+1);
            double result = a+b;
            arr[(int) (i % 100)] = result;
        }
        long stop = System.currentTimeMillis();
        System.out.println("Thread worked for " + (stop - start));
    }
}

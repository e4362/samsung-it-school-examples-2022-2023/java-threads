public class Main {
    public static void main(String[] args) throws InterruptedException {
        class A{
            long sum = 0;
        }
        A a = new A();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000000; i++){
                    synchronized (a){
                        a.sum += 100;
                    }
                }
            }
        });
        t1.start();
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000000; i++){
                    synchronized (a){
                        a.sum -= 100;
                    }

                }
            }
        });
        t2.start();

        t1.join();
        t2.join();

        System.out.println(a.sum);
    }
}